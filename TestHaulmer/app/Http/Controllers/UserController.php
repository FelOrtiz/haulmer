<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use JWTAuth;

class UserController extends Controller
{
	protected $user;

	public function __construct()
	{
		$this->user = JWTAuth::parseToken()->authenticate();
	}

	public function update(Request $request)
	{
		$this->user->name = $request->name;
		$this->user->email = $request->email;
		$this->user->password = bcrypt($request->password);
		$updated = $this->user->save();

		if($updated)
		{
			return response()->json([
				'success' => true,
			], 200);
		}
		else
		{
			return response()->json([
				'success' => false,
				'message' => 'Sorry, user could not be updated'
			], 400);
		}
	}

	public function destroy()
	{
		$deleted = $this->user->delete();

		if($deleted)
		{
			return response()->json([
				'success' => true,
			], 200);
		}
		else
		{
			return response()->json([
				'success' => false,
				'message' => 'Sorry, user could not be deleted'
			], 400);
		}
	}
}
