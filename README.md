# Test


## Instructions

1. Clone this project and CD (change directory) into the `TestHaulmer` folder.
2. Run the following command: `docker-compose up -d`.
3. Run the following command: `docker exec -it testhaulmer_app_1 /bin/bash` to get access over the bash container.
4. After that run: `cd..` to change directory to `/var/www` from `/var/www/html`.
5. Run:`php artisan migrate` to create the tables.
6. Finally run: `exit` to log out from container's bash.
7. Once you finish using the API run: `docker-compose down` to shut down the services and remove the images.

## Usage

This table is a short representation of how you should use the API, for further details please see `api.raml`. 

Type on your browser: `localhost:8081/api/{endpoint}` in order to use the API.

Endpoint	| HTTP Method	| Body / Query parameters	| JWT Token Auth 			|
------------| --------------| --------------------------| --------------------------|
`/new`		| POST			| name, email, password		| NO						|
`/login`	| POST			| email, password			| NO						|
`/logout`	| GET			| token						| YES, as query parameter	|
`/me`		| GET			| token						| YES, as query parameter	|
`/user`		| PUT			| name, email, password		| YES, as Bearer token		|
`/user`		| DELETE		| 							| YES, as Bearer token		|

